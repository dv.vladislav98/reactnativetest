import React from 'react';
import { View, ActivityIndicator, StyleSheet, RefreshControl, FlatList } from 'react-native';
import { ListItem } from 'react-native-elements'

import { getTicket } from './home.api';

class HomeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.onRefresh = this.onRefresh.bind(this);
    }

    state = {
        mass: [],
        keys: [],
        refreshing: false,
        isFetching: false
    };

    componentDidMount() {
        this.getData();
        this.interval = setInterval(() => {
            this.getData();
            this.setState({
                isFetching: false
            })
        }, 5000);
    }

    componentWillMount() {
        clearInterval(this.interval);
    }

    getData() {
        console.log(new Date);
        getTicket().then((data) => {
            this.setState({
                mass: Object.values(data),
                keys: Object.keys(data),
                refreshing: false,
                isFetching: true
            });
        }).catch((error) => {
            console.log(error);
        });
    }

    onRefresh() {
        this.setState({refreshing: true, isFetching: false});
        this.getData();
    }

    render () {
        if (this.state.isFetching) {
            return (
                <FlatList
                    data={this.state.mass}
                    extraData={this.state}
                    refreshControl={
                        <RefreshControl
                            refreshing={false}
                            onRefresh={this.onRefresh}
                        />
                    }
                    renderItem={({ item, index }) => (
                        <ListItem
                            topDivider
                            title={this.state.keys[index]}
                            subtitle={`${item.last} ${item.highestBid} ${item.percentChange}`}
                        />
                    )}
                    keyExtractor = { item => item.id.toString() }
                />
            )
        } else {
            return (
                <View stlye={styles.container} >
                    <ActivityIndicator size="large"/>
                </View>
            )
        }
    };
}

const styles = StyleSheet.create({
    subtitleView: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingTop: 5
    },
    ratingText: {
        paddingLeft: 10,
        color: 'grey'
    },
    container: {
        flex: 1
    }
});

export default HomeScreen;

/* <Button onPress={/*() => {this.props.find()} для передачи параметров () => {this.props.onClick(3)}}/> */
