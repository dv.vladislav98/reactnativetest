export const getTicket = () => {
    return fetch('https://poloniex.com/public?command=returnTicker')
    .then((response) => response.json())
    .then((responseJson) => {
        return responseJson;
    })
    .catch((error) =>{
        console.error(error);
    });
}