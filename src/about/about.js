import React from 'react';
import { StyleSheet, SafeAreaView, ScrollView, View, Button, Text } from 'react-native';
import { Icon } from 'react-native-elements';

class AboutScreen extends React.Component {

    constructor(props) {
      super(props);
      this.onClick = this.onClick.bind(this);
      this.goHomePage = this.goHomePage.bind(this);
    }

    state = {
        count: 1,
    };

    onClick() {
        this.setState({
            count: this.state.count + 1
        });
    }

    goHomePage() {
        this.props.navigation.navigate('Home');
    }

    render () {
        return (
            <SafeAreaView>
                <ScrollView contentInsetAdjustmentBehavior="automatic">
                <View>
                    <Icon name="home" />
                    <Text>FirstView</Text>
                    <Button title="Click on me" onPress={this.onClick}/>
                    <Button title="Go home page" onPress={this.goHomePage}/>
                    <Text>{this.state.count}</Text>
                </View>
                <View>
                    <Text>SecondView</Text>
                </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    firstView: {
        padding: 5,
    },
    secondView: {
        padding: 5,
    },
});

export default AboutScreen;