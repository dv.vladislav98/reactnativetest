import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from './src/home/home';
import AboutScreen from './src/about/about';

const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        headerTitle: 'Home',
        headerTitleStyle: {
          fontWeight: '500',
          fontSize: 24,
          color: 'white'
        },
        headerStyle: {
          backgroundColor: '#2089dc',
        },
        headerTintColor: 'white'
      }
    },
    About: {
      screen: AboutScreen,
      navigationOptions: {
        headerTitle: 'About',
        headerTitleStyle:{
          fontWeight: '500',
          fontSize: 24,
          color: 'white'
        },
        headerStyle:{
          backgroundColor: '#2089dc',
        },
      }
    },
  },
  {
    initialRouteName: 'About',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
